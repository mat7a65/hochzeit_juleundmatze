///////////////////////// ------- Flower ------- /////////////////////////
var button_flower = document.getElementById('click_flower');
var modal_flower = document.getElementById("modal_flower");
var close_flower = document.getElementById("close_flower");
var close_flower_x = document.getElementById("close_flower_x");


// "Button-Open"
if(button_flower){
    button_flower.addEventListener('click', function () {modal_flower.style.display = "block"});
}

// "Close-Button"
if(close_flower){
    close_flower.addEventListener('click', function () {
        modal_flower.style.display = "none"});
}

// "Close-Button X"
if(close_flower_x){
    close_flower_x.addEventListener('click', function () {
        modal_flower.style.display = "none"});
}

///////////////////////// ------- Flower ------- ////////////////////////

///////////////////////// ------- Sekt ------- ////////////////////////
var button_sekt = document.getElementById("click_sekt");
var modal_sekt = document.getElementById("modal_sekt");
var close_sekt = document.getElementById("close_sekt");
var close_sekt_x = document.getElementById("close_sekt_x");

// "Button-Open"
if(button_sekt){
    button_sekt.addEventListener('click', function () {modal_sekt.style.display = "block"});
}

// "Close-Button"
if(close_sekt){
    close_sekt.addEventListener('click', function () {
        modal_sekt.style.display = "none"});
}

// "Close-Button X"
if(close_sekt_x){
    close_sekt_x.addEventListener('click', function () {
        modal_sekt.style.display = "none"});
}

///////////////////////// ------- Sekt ------- ////////////////////////

///////////////////////// ------- Salat ------- ////////////////////////
var button_salat = document.getElementById("click_salat");
var modal_salat = document.getElementById("modal_salat");
var close_salat = document.getElementById("close_salat");
var close_salat_x = document.getElementById("close_salat_x");

// "Button-Open"
if(button_salat){
    button_salat.addEventListener('click', function () {modal_salat.style.display = "block"});
}

// "Close-Button"
if(close_salat){
    close_salat.addEventListener('click', function () {
        modal_salat.style.display = "none"});
}

// "Close-Button X"
if(close_salat_x){
    close_salat_x.addEventListener('click', function () {
        modal_salat.style.display = "none"});
}
///////////////////////// ------- Salat ------- ////////////////////////


///////////////////////// ------- Dessert ------- ////////////////////////
var button_dessert = document.getElementById("click_dessert");
var modal_dessert = document.getElementById("modal_dessert");
var close_dessert = document.getElementById("close_dessert");
var close_dessert_x = document.getElementById("close_dessert_x");

// "Button-Open"
if(button_dessert){
    button_dessert.addEventListener('click', function () {modal_dessert.style.display = "block"});
}

// "Close-Button"
if(close_dessert){
    close_dessert.addEventListener('click', function () {
        modal_dessert.style.display = "none"});
}

// "Close-Button X"
if(close_dessert_x){
    close_dessert_x.addEventListener('click', function () {
        modal_dessert.style.display = "none"});
}
///////////////////////// ------- Dessert ------- ////////////////////////


///////////////////////// ------- Music ------- ////////////////////////
var button_music = document.getElementById("click_music");
var modal_music = document.getElementById("modal_music");
var close_music = document.getElementById("close_music");
var close_music_x = document.getElementById("close_music_x");

// "Button-Open"
if(button_music){
    button_music.addEventListener('click', function () {modal_music.style.display = "block"});
}

// "Close-Button"
if(close_music){
    close_music.addEventListener('click', function () {
        modal_music.style.display = "none"});
}

// "Close-Button X"
if(close_music_x){
    close_music_x.addEventListener('click', function () {
        modal_music.style.display = "none"});
}
///////////////////////// ------- Music ------- ////////////////////////


///////////////////////// ------- Photo ------- ////////////////////////
var button_photo = document.getElementById("click_photo");
var modal_photo = document.getElementById("modal_photo");
var close_photo = document.getElementById("close_photo");
var close_photo_x = document.getElementById("close_photo_x");


// "Button-Open"
if(button_photo){
    button_photo.addEventListener('click', function () {modal_photo.style.display = "block"});
}

// "Close-Button"
if(close_photo){
    close_photo.addEventListener('click', function () {
        modal_photo.style.display = "none"});
}

// "Close-Button X"
if(close_photo_x){
    close_photo_x.addEventListener('click', function () {
        modal_photo.style.display = "none"});
}
///////////////////////// ------- Photo ------- ////////////////////////


///////////////////////// ------- Muscle ------- ////////////////////////
var button_muscle = document.getElementById("click_muscle");
var modal_muscle = document.getElementById("modal_muscle");
var close_muscle = document.getElementById("close_muscle");
var close_muscle_x = document.getElementById("close_muscle_x");

// "Button-Open"
if(button_muscle){
    button_muscle.addEventListener('click', function () {modal_muscle.style.display = "block"});
}

// "Close-Button"
if(close_muscle){
    close_muscle.addEventListener('click', function () {
        modal_muscle.style.display = "none"});
}

// "Close-Button X"
if(close_muscle_x){
    close_muscle_x.addEventListener('click', function () {
        modal_muscle.style.display = "none"});
}
///////////////////////// ------- Muscle ------- ////////////////////////

///////////////////////// ------- Impressum ------- ////////////////////////
var button_impressum = document.getElementById("click_impressum");
var modal_impressum = document.getElementById("modal_impressum");
var close_impressum = document.getElementById("close_impressum");
var close_impressum_x = document.getElementById("close_impressum_x");

// "Button-Open"
if(button_impressum){
    button_impressum.addEventListener('click', function () {modal_impressum.style.display = "block"});
}

// "Close-Button"
if(close_impressum){
    close_impressum.addEventListener('click', function () {
        modal_impressum.style.display = "none"});
}

// "Close-Button X"
if(close_impressum_x){
    close_impressum_x.addEventListener('click', function () {
        modal_impressum.style.display = "none"});
}
///////////////////////// ------- Muscle ------- ////////////////////////

/*

// "Close-Button BG"
window.onclick = function (event) {
    if (event.target.className == 'modal-background') {
        modal_flower.style.display = 'none';
        modal_sekt.style.display = 'none';
        modal_salat.style.display = 'none';
        modal_dessert.style.display = 'none';
        modal_music.style.display = 'none';
        modal_photo.style.display = 'none';
        modal_muscle.style.display = 'none';
        modal_teilnehmen.style.display = 'none';
    }
};

 */

///////////////////////// ------- Teilnehmen ------- ////////////////////////
// Button-Teilnehmen
var modal_teilnehmen = document.getElementById("modal_teilnehmen");
var close_teilnehmen = document.getElementById("close_teilnehmen");
var close_teilnehmen_x = document.getElementById("close_teilnehmen_x");
var close_eintragen = document.getElementById("close_eintragen");


///////////////////////////////////////

// "Close-Button"
if(close_teilnehmen){
    close_teilnehmen.addEventListener('click', function () {
        modal_teilnehmen.style.display = 'none'});
}

// "Close-Button X"
if(close_teilnehmen_x){
    close_teilnehmen_x.addEventListener('click', function () {
        modal_teilnehmen.style.display = 'none'});
}

// "Close-ButtonII"
if(close_eintragen){
    close_eintragen.addEventListener('click', function() {
        modal_teilnehmen.style.display  = "none";
    });
}

window.onclick = function (event) {
    if (event.target.className === "button is-success") {
        modal_teilnehmen.style.display = "block";
    }
};
///////////////////////// ------- Teilnehmen ------- ////////////////////////

/////////ScrollButton///////////////
//Get the button:
mybutton = document.getElementById("topButton");

// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function () {
    scrollFunction()
};

function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        mybutton.style.display = "block";
    } else {
        mybutton.style.display = "none";
    }
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
}

/////////ScrollButton///////////////
